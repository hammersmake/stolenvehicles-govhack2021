package main

import "github.com/gin-gonic/gin"

func main() {
	r := gin.Default()

	r.GET("/plate/:id", func(c *gin.Context) {
		iden := c.Param("id")
		c.JSON(200, gin.H{
			"data": string(ReadOneDB(iden)),
		})
	})
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"data": ReadDB(),
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

//ReadCertainLine("stolenvehicles.csv")
//ReadCertainLine()
//WriteDB()
//fmt.Println(ReadOneDB())
//fmt.Println(ReadDB())
