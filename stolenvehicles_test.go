package main

import (
	"testing"
)

func Test_Readcsv(t *testing.T) {
	got := Readcsv("stolenvehiclesexample.csv")
	want := "id,colour,brand,detail,year,type,date,location"
	if got != want {
		t.Errorf("Got '%s' but wanted '%s'", got, want)
	}
}

func Test_ReadCertainLine(t *testing.T) {
	got := ReadCertainLine(1, "stolenvehicles.csv")
	want := "1005W,Yellow,Briford,TRAILER,2001,Trailer,2021-04-06,Auckland City"
	if got != want {
		t.Errorf("Got '%s' but wanted '%s'", got, want)
	}
}
