package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

type CarEntry struct {
	ID       string `gorm:"primaryKey"`
	Colour   string
	Brand    string
	Detail   string
	Year     string
	Type     string
	Date     string
	Location string
}

func Readcsv(filename string) string {
	csvFile, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}
	return string(csvFile)
}

func WriteDB() {

	dsn := "sqlserver://" + os.Getenv("dbuser") + ":" + os.Getenv("dbpass") + "@stolenv.database.windows.net:1433?database=stolen"
	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println(err)
	}
	db.AutoMigrate(&CarEntry{})
}

func WriteEntryDB() {
	dsn := "sqlserver://" + os.Getenv("dbuser") + ":" + os.Getenv("dbpass") + "@stolenv.database.windows.net:1433?database=stolen"
	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println(err)
	}
	fmt.Println("opening csv")
	csvFile, _ := os.Open("stolenvehicles.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var entry []CarEntry
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		stolen := append(entry, CarEntry{
			ID:       line[0],
			Colour:   line[1],
			Brand:    line[2],
			Detail:   line[3],
			Year:     line[4],
			Type:     line[5],
			Date:     line[6],
			Location: line[7],
		})
		fmt.Println(stolen)
		db.Create(stolen)
		stolenJson, _ := json.Marshal(stolen)
		fmt.Println(string(stolenJson))
	}
}

func ReadOneDB(numplate string) string {
	dsn := "sqlserver://" + os.Getenv("dbuser") + ":" + os.Getenv("dbpass") + "@stolenv.database.windows.net:1433?database=stolen"
	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println(err)
	}
	var entry []CarEntry
	db.First(&entry, "id = ?", numplate)
	out, err := json.Marshal(entry)
	if err != nil {
		panic (err)
	}
	return(string(out))
}

func ReadDB() string {
	dsn := "sqlserver://" + os.Getenv("dbuser") + ":" + os.Getenv("dbpass") + "@stolenv.database.windows.net:1433?database=stolen"
	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println(err)
	}
	var entry []CarEntry
	db.Find(&entry)

  out, err := json.Marshal(entry)
  if err != nil {
  	panic (err)
  }

	return(string(out))

}
