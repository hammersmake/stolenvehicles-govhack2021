module example.com/stolenvehicles

go 1.16

require (
	github.com/denisenkom/go-mssqldb v0.10.0 // indirect
	gorm.io/driver/postgres v1.1.0 // indirect
	gorm.io/driver/sqlserver v1.0.8 // indirect
	gorm.io/gorm v1.21.13 // indirect
)
