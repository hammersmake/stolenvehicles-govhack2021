Project for GovHack2021 for the challange Engaging with SmartView-Go

Stolen Vehicles

App that accepts photos of registration numbers and returns back if the car is stolen or not. 

So when you out and about and see a dodgy car - and if it's safe, snap a photo, and the app will check the number and return back the result if it is stolen or not. Uses character recognition on the photo.

Backend code is written in Golang. 

Improve the dataset by feeding it into database with extra data (updated, notes etc)

Can I get data of stolen vehicles that is older than 

The csv data can be found here: 

https://www.police.govt.nz/can-you-help-us/stolen-vehicles
